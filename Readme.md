# Learning Services Test Automation Framework


### Points of contacts
Kyle.lutze@pearson.com
Giri.palanivel@pearson.com
Hitesh.shah@pearson.com


### About Framework

This is an unified framework in Learning Services. Which has common utilities for UI automation(Selenium with Java, Gradle, TestNg), 
REST api testing(Rest Assured), Cloud based test execution(Sauce labs), CI support(Jenkins), 
Chrome Mobile Emulation(using userAgent), Customized test reporting.

This core framework builded as jar file and can be used as references for other project.

build cmd: gradle build install

And it has to be added as dependency in project framework as below:
compile ('com.pearson:test-automation-framework:x.x.x-SNAPSHOT')

For more reference:
https://confluence.pearson.com/confluence/display/LSSQE/Test+Framework+-+Overview
https://confluence.pearson.com/confluence/display/LSSQE/Test+Framework+-+Setup
https://confluence.pearson.com/confluence/display/LSSQE/Test+Framework+-+Running


### How to contribute code
1. create a ticket
2. create a pull request from feature branch


### Stuff
JIRA project: http://jira.pearsoncmg.com/jira/browse/LSTF
code repository: https://bitbucket.pearson.com/projects/TT/repos/test-automation-framework
